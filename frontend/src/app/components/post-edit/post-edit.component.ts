import { Component, Inject, OnInit } from '@angular/core';
import { Post } from '../../models/post/post';
import { SnackBarService } from '../../services/snack-bar.service';
import { switchMap, takeUntil } from 'rxjs/operators';
import { PostService } from '../../services/post.service';
import { ImgurService } from '../../services/imgur.service';
import { Subject } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-post-edit',
    templateUrl: './post-edit.component.html',
    styleUrls: ['./post-edit.component.sass']
})
export class PostEditComponent implements OnInit {

    public post: Post;
    public imageUrl: string;
    public imageFile: File;
    public loading = false;
    private unsubscribe$ = new Subject<void>();

    constructor(
        public dialogRef: MatDialogRef<PostEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Post,
        private snackBarService: SnackBarService,
        private postService: PostService,
        private imgurService: ImgurService) {
    }

    public ngOnInit(): void {
        this.post = this.data;
        this.imageUrl = this.post.previewImage;
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
        this.post.previewImage = undefined;
    }

    public sendPost() {
        const postSubscription = !this.imageFile
            ? this.postService.updatePost(this.post)
            : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.body.data.link;
                    return this.postService.updatePost(this.post);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.data = respPost.body;
                this.loading = false;
                this.dialogRef.close();
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public onNoClick(): void {
        this.dialogRef.close();
    }
}
