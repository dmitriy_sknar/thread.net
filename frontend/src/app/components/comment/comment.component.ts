import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from '../../models/user';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnInit {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Output() public deleteCommentEvent = new EventEmitter<Comment>();
    @Output() public updateCommentEvent = new EventEmitter<Comment>();
    @Output() public likeCommentEvent = new EventEmitter<any>();

    public showEditComment = false;
    public showUserCommands = false;

    public ngOnInit() {
        this.evaluateShowUserCommands();
    }

    public toggleShowEditComment() {
        this.showEditComment = !this.showEditComment;
    }

    public updateComment() {
        this.updateCommentEvent.emit(this.comment);
        this.toggleShowEditComment();
    }

    public evaluateShowUserCommands() {
        if (this.currentUser.id === this.comment.author.id) {
            this.showUserCommands = !this.showUserCommands;
        }
    }

    public deleteComment() {
        this.deleteCommentEvent.emit(this.comment);
    }

    public likeComment() {
        this.likeCommentEvent.emit(
            {
                comment: this.comment,
                isLike: true
            });
    }

    public dislikeComment() {
        this.likeCommentEvent.emit(
            {
                comment: this.comment,
                isLike: false
            });
    }

    public commentLikes() {
        return (this.comment.reactions.filter((p) => p.isLike === true)).length;
    }

    public commentDislikes() {
        return (this.comment.reactions.filter((p) => p.isLike === false)).length;
    }
}
