import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, of, pipe, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostService } from '../../services/post.service';
import { MatDialog } from '@angular/material/dialog';
import { PostEditComponent } from '../post-edit/post-edit.component';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnInit, OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;
    @Output() public onDelete = new EventEmitter<Post>();

    public showComments = false;
    public showUserCommands = false;
    public newComment = {} as NewComment;
    public usersLikedPostString = '';
    public usersDisLikedPostString = '';
    private usersDisLikedPost: string[] = [];
    private usersLikedPost: string[] = [];

    private unsubscribe$ = new Subject<void>();

    public constructor(
        public dialog: MatDialog,
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private postService: PostService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private userService: UserService
    ) {
    }

    public ngOnInit() {
        this.evaluateShowUserCommands();
        this.getUsersLikedPostList();
        this.getUsersDisLikedPostList();
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public postLikes(): number {
        return (this.post.reactions.filter((p) => p.isLike === true)).length;
    }

    public postDislikes(): number {
        return (this.post.reactions.filter((p) => p.isLike === false)).length;
    }

    public deletePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.postService.deletePost(this.post.id)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.notifyDeleted(post)));
            return;
        }

        this.postService.deletePost(this.post.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.notifyDeleted(post)));
    }

    public editPost() {
        const dialogRef = this.dialog.open(PostEditComponent, {
            height: 'auto',
            width: '50%',
            data: this.post
        });

        dialogRef.afterClosed().subscribe((result) => {
            console.log('The dialog was closed');
            if (result) this.post = result;
        });
    }

    public likePost() {
        this.doLikePost(true);
    }

    public dislikePost() {
        this.doLikePost(false);
    }

    public doLikePost(isLike: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp, isLike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser, isLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.post = post;
                this.clearReactions();
                this.getUsersLikedPostList();
                this.getUsersDisLikedPostList();
            });
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public updateComment(updateComment: Comment) {
        this.commentService
            .updateComment(updateComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        updateComment = resp.body;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
        this.snackBarService.showUsualMessage('Comment updated');
    }

    public deleteComment(deleteComment: Comment) {
        this.commentService.deleteComment(deleteComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((resp) => {
                    this.post.comments = this.sortCommentArray(
                        this.post.comments.filter((c) => c.id !== resp.body.id)
                    );
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public doLikeComment(eventData: any) {
        let comment = eventData.comment;
        const isCommentLike = eventData.isLike;

        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(comment, userResp, isCommentLike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((c) => (eventData.comment = c));

            return;
        }

        this.likeService
            .likeComment(comment, this.currentUser, isCommentLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((c) => (comment = c));
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public evaluateShowUserCommands() {
        if (!this.currentUser || !this.currentUser) {
            this.showUserCommands = false;
            return;
        }

        this.showUserCommands = this.currentUser.id === this.post.author.id
            ? this.showUserCommands = true
            : this.showUserCommands = false;
    }

    public getUsersLikedPostList() {
        const userIds = (this.post.reactions
            .filter((p) => p.isLike === true))
            .map((r) => r.user.id);

        if (userIds.length === 0) return;

        userIds.map((id) => {
            this.userService.getUserById(id)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((resp) => {
                    const name = resp.body.userName;
                    this.usersLikedPost.push(name);
                    this.usersLikedPostString = 'Reaction from: ' + this.usersLikedPost.join(', ');
                    console.log('Loaded names: ' + this.usersLikedPostString);
                });
        });
    }

    public getUsersDisLikedPostList() {
        const userIds = (this.post.reactions
            .filter((p) => p.isLike === false))
            .map((r) => r.user.id);

        if (userIds.length === 0) return;

        userIds.map((id) => {
            this.userService.getUserById(id)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((resp) => {
                    const name = resp.body.userName;
                    this.usersDisLikedPost.push(name);
                    this.usersDisLikedPostString = 'Reaction from: ' + this.usersDisLikedPost.join(', ');
                    console.log('Loaded names: ' + this.usersDisLikedPostString);
                });
        });
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    private notifyDeleted(post: Post) {
        this.onDelete.emit(post);
    }

    private clearReactions() {
        this.usersLikedPostString = '';
        this.usersDisLikedPostString = '';
        this.usersDisLikedPost = [];
        this.usersLikedPost = [];
    }
}
