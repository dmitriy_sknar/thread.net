import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { CommentService } from './comment.service';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService,
                       private postService: PostService,
                       private commentService: CommentService) {}

    public likePost(post: Post, currentUser: User, isLike: boolean) {
        const innerPost = post;
        const innerIsLike = isLike;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: innerIsLike,
            userId: currentUser.id
        };
        let hasCurrentReaction = false;
        // make cache for rollback
        const cache = innerPost.reactions;
        // update current array instantly
        const hasOppositeReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id && x.isLike !== innerIsLike);
        if (hasOppositeReaction) {
            innerPost.reactions = innerPost.reactions.filter((x) => !(x.user.id === currentUser.id && x.isLike === !innerIsLike));
            hasCurrentReaction = false;
        } else {
            hasCurrentReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id && x.isLike === innerIsLike);
        }
        innerPost.reactions = hasCurrentReaction
            ? innerPost.reactions.filter((x) => !(x.user.id === currentUser.id && x.isLike === innerIsLike))
            : innerPost.reactions.concat({isLike: innerIsLike, user: currentUser});
        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = cache;
                return of(innerPost);
            })
        );
    }

    public likeComment(comment: any, currentUser: User, isLike: boolean) {
        const innerComment = comment;
        const innerIsLike = isLike;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: innerIsLike,
            userId: currentUser.id
        };
        let hasCurrentReaction = false;
        // make cache for rollback
        const cache = innerComment.reactions;
        // update current array instantly
        const hasOppositeReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id && x.isLike !== innerIsLike);
        if (hasOppositeReaction) {
            innerComment.reactions = innerComment.reactions.filter((x) => !(x.user.id === currentUser.id && x.isLike === !innerIsLike));
            hasCurrentReaction = false;
        } else {
            hasCurrentReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id && x.isLike === innerIsLike);
        }
        innerComment.reactions = hasCurrentReaction
            ? innerComment.reactions.filter((x) => !(x.user.id === currentUser.id && x.isLike === innerIsLike))
            : innerComment.reactions.concat({isLike: innerIsLike, user: currentUser});
        return this.commentService.likeComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                innerComment.reactions = cache;
                return of(innerComment);
            })
        );
    }
}
