import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';
import { Post } from '../models/post/post';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) {}

    public createComment(post: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, post);
    }

    public updateComment(comment: Comment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/${comment.id}`, comment);
    }

    public deleteComment(deleteComment: Comment) {
        return this.httpService.deleteFullRequest<Comment>(`${this.routePrefix}/${deleteComment.id}`);
    }

    public likeComment(reaction: NewReaction) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/like`, reaction);
    }
}
