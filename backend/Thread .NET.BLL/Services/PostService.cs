﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task<PostDTO> UpdatePost(PostDTO dto)
        {
            var post = await _context.Posts.Select(p => p).Where(p => p.Id == dto.Id).FirstOrDefaultAsync();
            if (post != null)
            {
                post.Body = dto.Body;

                var image = await _context.Images.Select(i => i).Where(i => i.URL.Equals(dto.PreviewImage))
                    .FirstOrDefaultAsync();
                if (image == null && !string.IsNullOrEmpty(dto.PreviewImage))
                {
                    var newImage = _context.Images.Add(new Image
                    {
                        URL = dto.PreviewImage
                    }).Entity;
                    post.Preview = newImage;
                } 
                else if (string.IsNullOrEmpty(dto.PreviewImage))
                {
                    post.Preview = null;
                    post.PreviewId = null;
                }
                
                _context.Posts.Update(post);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new KeyNotFoundException();
            }

            var updatedPost = await _context.Posts
                .Include(p => p.Author)
                .ThenInclude(author => author.Avatar)
                .FirstAsync(p => p.Id == dto.Id);

            var updatedPostDto = _mapper.Map<PostDTO>(updatedPost);
            return updatedPostDto;
        }

        public async Task<PostDTO> DeletePost(PostDTO postDto)
        {
            var post = await _context.Posts.Select(p => p).Where(p => p.Id == postDto.Id).FirstOrDefaultAsync();
            if (post != null)
            {
                var comments = await _context.Comments.Select(c => c).Where(c => c.PostId == postDto.Id).ToListAsync();
                _context.Comments.RemoveRange(comments);

                var likes = await _context.PostReactions.Select(r => r).Where(r => r.PostId == postDto.Id).ToListAsync();
                _context.PostReactions.RemoveRange(likes);

                _context.Posts.Remove(post);
               await _context.SaveChangesAsync();
            }
            else
            {
                throw new KeyNotFoundException();
            }

            return _mapper.Map<PostDTO>(post); 
        }
    }
}
