﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        public LikeService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId 
                                                          && x.PostId == reaction.EntityId);

            if (likes.Any())
            {
                var dbReaction = likes.FirstOrDefault();

                _context.PostReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                //exit only if same type of like otherwise proceed further and save opposite reaction
                if (dbReaction.IsLike == reaction.IsLike) return;
            }

            var postReaction = new DAL.Entities.PostReaction {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            };

            _context.PostReactions.Add(postReaction);
            await _context.SaveChangesAsync();
        }

        public async Task LikeComment(NewReactionDTO reaction)
        {
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId
                                                          && x.CommentId == reaction.EntityId);

            if (likes.Any())
            {
                var dbReaction = likes.FirstOrDefault();

                _context.CommentReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                //exit only if same type of like otherwise proceed further and save opposite reaction
                if (dbReaction.IsLike == reaction.IsLike) return;
            }

            var commentReaction = new DAL.Entities.CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            };

            _context.CommentReactions.Add(commentReaction);
            await _context.SaveChangesAsync();
        }
    }
}